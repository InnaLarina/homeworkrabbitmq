﻿using Bogus;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleRabbit
{
    class Program
    {
        static Random random = new Random();
        const int maxAge = 125;
        const int countMessage = 100;
        static Faker<User> CreateFaker()
        {
            var customersFaker = new Faker<User>("ru")
                .CustomInstantiator(f => new User()
                {
                    Age = random.Next(1, maxAge)
                })
                .RuleFor(u => u.Name, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "shrimp.rmq.cloudamqp.com", VirtualHost = "timbpplm", UserName = "timbpplm", Password= "BMpFd5Baw0G5VaZAN0YhNPq0_ZmeZllL" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "SampleHW",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                for (int i = 0; i < countMessage; i++)
                {
                  User user = CreateFaker();
                  string message = JsonConvert.SerializeObject(user);
                  var body = Encoding.UTF8.GetBytes(message);

                  channel.BasicPublish(exchange: "",
                                     routingKey: "SampleHW",
                                     basicProperties: null,
                                     body: body);
                  Console.WriteLine(" [x] Sent {0}", message);
                  Thread.Sleep(1000); 
                }
            }

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
        class User 
        {
            public string Name { get; set; }
            public string Phone { get; set; }
            public int Age { get; set; }
        }
    }
}
