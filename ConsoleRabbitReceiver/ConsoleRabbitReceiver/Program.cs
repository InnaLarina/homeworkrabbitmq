﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRabbitReceiver
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "shrimp.rmq.cloudamqp.com", VirtualHost = "timbpplm", UserName = "timbpplm", Password = "BMpFd5Baw0G5VaZAN0YhNPq0_ZmeZllL" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "SampleHW",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    var user = JsonConvert.DeserializeObject<User>(message);
                    Console.WriteLine(" [x] Received {0}", user.ToString());
                };
                channel.BasicConsume(queue: "SampleHW",
                                     autoAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }
        class User
        {
            public User(string name, string phone, int age)
            {
                Name = name;
                Phone = phone;
                Age = age;
            }

            public string Name { get; set; }
            public string Phone { get; set; }
            public int Age { get; set; }
            public override string ToString()
            {
                return $"User: Name {Name}, Phone {Phone}, Age {Age}";
            }
        }
    }
}
